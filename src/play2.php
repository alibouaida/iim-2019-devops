
<?php

if (isset($_POST['formconnexion'])) {
    $mdpconnect = $_POST['mdpconnect'];

    if ($mdpconnect == "triomphant") {
        $erreur = "OH MY GOD !!<br /> <a href=\"play3.php\" class=\"link\" >GO</a>";
    } else {
        $erreur = "Essaye encore ";
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Grayscale - Start Bootstrap Theme</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/grayscale.min.css" rel="stylesheet">
    <link href="css/grayscale.css" rel="stylesheet">

</head>
<body>
<br>
<br>
<br>
<br>
<br>
<br>
<div class="container d-flex h-100 align-items-center">
    <div class="play">
    <div class="mx-auto text-center ">
        <h2 class="text-white mb-4">Carnet de bord n°2</h2>
        <h5 class="text-white-50 mx-auto mt-2 mb-5" ><p>Le <strong>T</strong>riomphant c’était quelque chose à l’époque, le premie<strong>R</strong> d’une nouvelle générat<strong>I</strong>on. C’était c<strong>O</strong>mme monter les <strong>M</strong>arches-rouges à Cannes <strong>P</strong>our nous. Mais bon souvent, on se posait des questions sur notre <strong>H</strong>abilité à gérer le poisson. Le 4 février 2009 on est “Entré en cont<strong>A</strong>ct brièvement” avec un sous-Marin Anglais, le HMS vanguard. Fin c’est ce qu’o<strong>N</strong> raconté les supérieurs, la vérité c’est qu’on c’est carrémen<strong>T</strong> rentré dedans avec les britishs. On a du faire se détourner quatre fichus navires de leurs trajectoires pour nous remorquer jusqu'à la base. Autant te dire que tout l’équipage c’est pris le plus gros savon de tous les temps. En même temps on peut pas vraiment leur en vouloir, la moindre de nos erreurs leur coûtent des millions.</p>
        </h5>
        <h4 class="text-white mb-4">Enfin bon, je continue pour voir si tu es finaud, j’ai caché un mot dans ce message dis moi lequel.</h4>
        <form class="form-signin pt-3" method="POST" action="">
            <label for="inputPassword" class="sr-only">Réponse</label>
            <input type="" name="mdpconnect" id="inputPassword" class="form-control" placeholder="Réponse"
                   required><br>
            <button class="btn btn-lg btn-danger btn-block" name="formconnexion" type="submit">Entrer</button>
        </form>
    </div>
    </div>
</div>
<br>
<?php
if (isset($erreur)) {
    echo '<h4 class="text-white mb-4">' . $erreur . " </h4>";
}
?>

</div>

<!-- Bootstrap core JavaScript -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Plugin JavaScript -->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for this template -->
<script src="js/grayscale.min.js"></script>

</body>

</html>

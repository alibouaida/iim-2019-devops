
<?php

if (isset($_POST['formconnexion'])) {
    $mdpconnect = $_POST['mdpconnect'];

    if ($mdpconnect == "drake") {
        $erreur = " Tricheur ..<br /> <a href=\"play3.php\" class=\"link\" >GO</a>";
    } else {
        $erreur = "Essaye encore ";
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Grayscale - Start Bootstrap Theme</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/grayscale.min.css" rel="stylesheet">
    <link href="css/grayscale.css" rel="stylesheet">

</head>
<body>
<br>
<br>
<br>
<br>
<br>
<br>
<div class="container d-flex h-100 align-items-center">
    <div class="mx-auto text-center">
        <h2 class="text-white mb-4">Carnet de bord n°3</h2>
        <h4 class="text-white mb-4">Voyons voir, tu es déjà un peu mieux que certaines personnes que j’ai rencontré. Un dernier petit test, et on parlera de ce qui t'amène ici, est ce que tu peux lire ce mot et me l’écrire?
        </h4>
        <img src="img/carte.png" class="img-fluid" alt="">
        <form class="form-signin pt-3" method="POST" action="">
            <label for="inputPassword" class="sr-only">Réponse</label>
            <input type="" name="mdpconnect" id="inputPassword" class="form-control" placeholder="Réponse"
                   required><br>
            <button class="btn btn-lg btn-danger btn-block" name="formconnexion" type="submit">Entrer</button>
        </form>
    </div>
</div>
<br>
<?php
if (isset($erreur)) {
    echo '<h4 class="text-white mb-4">' . $erreur . " </h4>";
}
?>

</div>

<!-- Bootstrap core JavaScript -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Plugin JavaScript -->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for this template -->
<script src="js/grayscale.min.js"></script>

</body>

</html>
